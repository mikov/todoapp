package com.example.todoapp.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.todoapp.database.TaskDBSchema.TaskTable;
import com.example.todoapp.model.Task;

import org.joda.time.DateTime;

import java.util.UUID;

public class TaskCursorWrapper extends CursorWrapper {

    public TaskCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Task getTask() {
        String uuidString = getString(getColumnIndex(TaskTable.Cols.UUID));
        String title = getString(getColumnIndex(TaskTable.Cols.TITLE));
        long date = getLong(getColumnIndex(TaskTable.Cols.DATE));
        int isDone = getInt(getColumnIndex(TaskTable.Cols.SOLVED));

        Task task = new Task(UUID.fromString(uuidString));
        task.setTitle(title);
        task.setDate(new DateTime(date));
        task.setDone(isDone != 0);

        return task;
    }
}
