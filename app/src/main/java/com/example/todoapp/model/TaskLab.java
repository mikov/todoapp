package com.example.todoapp.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.todoapp.database.TaskBaseHelper;
import com.example.todoapp.database.TaskCursorWrapper;
import com.example.todoapp.database.TaskDBSchema.TaskTable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskLab {

    private static TaskLab taskLab;
    private SQLiteDatabase database;

    private TaskLab(Context context) {
        Context mContext = context.getApplicationContext();
        database = new TaskBaseHelper(mContext).getWritableDatabase();
    }

    public static TaskLab get(Context context) {
        if (taskLab == null) {
            taskLab = new TaskLab(context);
        }
        return taskLab;
    }

    private static ContentValues getContentValues(Task task) {
        ContentValues values = new ContentValues();
        values.put(TaskTable.Cols.UUID, task.getId().toString());
        values.put(TaskTable.Cols.TITLE, task.getTitle());
        values.put(TaskTable.Cols.DATE, task.getDate().toString());
        values.put(TaskTable.Cols.SOLVED, task.isDone() ? 1 : 0);
        return values;
    }

    public void addTask(Task t) {
        ContentValues values = getContentValues(t);
        database.insert(TaskTable.NAME, null, values);
    }

    public void deleteTask(UUID id) {
        try {
            database.delete(
                    TaskTable.NAME,
                    TaskTable.Cols.UUID + " = ? ",
                    new String[]{id.toString()}
            );
        } catch (Exception e) {
            Log.d("deleteTask", e.toString());
        }
    }

    public List<Task> getTasks() {
        List<Task> tasks = new ArrayList<>();

        TaskCursorWrapper cursorWrapper = queryTasks(null, null);

        try {
            cursorWrapper.moveToFirst();
            while (!cursorWrapper.isAfterLast()) {
                tasks.add(cursorWrapper.getTask());
                cursorWrapper.moveToNext();
            }
        } finally {
            cursorWrapper.close();
        }
        return tasks;
    }

    public Task getTask(UUID id) {

        TaskCursorWrapper cursorWrapper = queryTasks(
                TaskTable.Cols.UUID + " = ?",
                new String[]{id.toString()}
        );

        try {
            if (cursorWrapper.getCount() == 0) {
                return null;
            }
            cursorWrapper.moveToFirst();
            return cursorWrapper.getTask();
        } finally {
            cursorWrapper.close();
        }
    }

    public void updateTask(Task t) {
        String uuidString = t.getId().toString();
        ContentValues values = getContentValues(t);

        database.update(TaskTable.NAME, values,
                TaskTable.Cols.UUID + " = ?",
                new String[]{uuidString});
    }

    private TaskCursorWrapper queryTasks(String whereClause, String[] whereArgs) {
        Cursor cursor = database.query(
                TaskTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new TaskCursorWrapper(cursor);
    }

}
