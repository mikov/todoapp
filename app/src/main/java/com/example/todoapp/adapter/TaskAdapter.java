package com.example.todoapp.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.todoapp.R;
import com.example.todoapp.model.Task;
import com.example.todoapp.ui.TaskPagerActivity;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskHolder> {
    private List<Task> tasksList;
    DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd MMM yyyy").withLocale(new Locale("ru"));

    public TaskAdapter(List<Task> tasks) {
        tasksList = tasks;
    }


    @NonNull
    @Override
    public TaskHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_task, viewGroup, false);
        return new TaskHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskHolder holder, int position) {
        Task task = tasksList.get(position);
        holder.bindTask(task);
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    public void setTasks(List<Task> tasks) {
        tasksList = tasks;
    }

    class TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.list_item_task_date_text_view) TextView dateTextView;
        @BindView(R.id.list_item_task_title_text_view) TextView titleTextView;
        @BindView(R.id.list_item_task_isdone_check_box) CheckBox isDoneCheckBox;
        private Task task1;

        TaskHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        void bindTask(Task task) {
            task1 = task;
            titleTextView.setText(task1.getTitle());
            dateTextView.setText(task1.getDate().toString(dateTimeFormatter));
            isDoneCheckBox.setChecked(task1.isDone());
        }

        @Override
        public void onClick(View v) {
            Intent intent = TaskPagerActivity.newIntent(v.getContext(), task1.getId());
            v.getContext().startActivity(intent);
        }
    }
}
