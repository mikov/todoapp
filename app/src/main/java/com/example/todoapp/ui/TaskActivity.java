package com.example.todoapp.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.example.todoapp.ui.fragments.TaskFragment;

import java.util.UUID;

public class TaskActivity extends SingleFragmentActivity {

    private static final String EXTRA_TASK_ID = "task_id";

    public static Intent newIntent(Context context, UUID taskId) {
        Intent intent = new Intent(context, TaskActivity.class);
        intent.putExtra(EXTRA_TASK_ID, taskId);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        UUID taskId = (UUID) getIntent().getSerializableExtra(EXTRA_TASK_ID);
        return TaskFragment.newInstance(taskId);
    }
}
