package com.example.todoapp.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.todoapp.R;
import com.example.todoapp.model.Task;
import com.example.todoapp.model.TaskLab;
import com.example.todoapp.ui.fragments.TaskFragment;

import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskPagerActivity extends AppCompatActivity {
    private static final String EXTRA_TASK_ID = "task_id";
    @BindView(R.id.activity_task_pager_view_pager) ViewPager viewPager;
    private List<Task> tasks;

    public static Intent newIntent(Context context, UUID taskId) {
        Intent intent = new Intent(context, TaskPagerActivity.class);
        intent.putExtra(EXTRA_TASK_ID, taskId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_pager);
        UUID taskId = (UUID) getIntent().getSerializableExtra(EXTRA_TASK_ID);
        ButterKnife.bind(this);
        tasks = TaskLab.get(this).getTasks();

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Task task = tasks.get(position);
                return TaskFragment.newInstance(task.getId());
            }

            @Override
            public int getCount() {
                return tasks.size();
            }
        });

        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getId().equals(taskId)) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
