package com.example.todoapp.ui.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.example.todoapp.R;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatePickerFragment extends DialogFragment {

    public static final String ARG_DATE = "date";
    public static final String EXTRA_DATE = "taskintent.date";

    @BindView(R.id.dialog_date_date_picker) DatePicker datePicker;

    public static DatePickerFragment newInstance(DateTime date) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DATE, date.minusMonths(1));

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        DateTime dateTime = (DateTime) getArguments().getSerializable(ARG_DATE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_date, null);
        ButterKnife.bind(this, view);
        datePicker.init(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), null);
        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.date_picker_title)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    DateTime dateTime1 = new DateTime().withDate(datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth());
                    sendResult(Activity.RESULT_OK, dateTime1);
                })
                .create();
    }

    private void sendResult(int resultCode, DateTime date) {
        if (getTargetFragment() == null) {
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATE, date);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }
}
