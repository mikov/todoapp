package com.example.todoapp.ui;

import android.support.v4.app.Fragment;

import com.example.todoapp.ui.fragments.TaskListFragment;

public class TaskListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new TaskListFragment();
    }
}
